---
# Proyectos 2021-1. Inteligencia Artificial II. 

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Clasificación de imagenes con residuos contamientantes](#proy1)
2. [Modelo de deeplearning para la clasificación de cancer pulmonar](#proy2)
3. [Ajuste de la calibración durante la recuperación del sistema CASSI](#proy3)
4. [mayo clinic](#proy4)




## Clasificación de imágenes para la detección de puntos críticos generados por la acumulación de residuos contaminantes. <a name="proy1"></a>

**Autores: Neider Smith Narvaez Carvajal (2170128)**

<img src="https://raw.githubusercontent.com/NeiderSmith/gb_vs_nogb/main/logo.jpg" style="width:700px;">

**Objetivo:Clasificar imagenes que sean posibles puntos criticos por la acumulación de residuos contaminantes.**

- Dataset:https://www.kaggle.com/datasets/apremeyan/garbage
- Modelo:CNN

[(code)](https://github.com/NeiderSmith/gb_vs_nogb/blob/main/Proyecto_IA2.ipynb) [(video)](https://www.youtube.com/watch?v=nfRQczDR_mE&feature=youtu.be) [(+info)](https://github.com/NeiderSmith/gb_vs_nogb/blob/main/Presentaci%C3%B3n_proyecto_IA_2.pdf)
---
## Modelos de deep learning para la clasificación de cáncer pulmonar  <a name="proy2"></a>

**Autores: Maria Paula Rodriguez, Jose Fabian Jimenez, Jorge Sandoval**

<img src="https://raw.githubusercontent.com/fabian017/Inteligencia-Artifical-2/main/banner-ia2.png" style="width:700px;">

**Objetivo:  crear una ayuda para los profesionales de la salud en la detección de cáncer pulmonar no microcítico.**

- Dataset:https://www.kaggle.com/datasets/mohamedhanyyy/chest-ctscan-images
- Modelo:Autoencoders, CNN, VGG19,InceptionResNetV2,EfficientNetB3

[(code)](https://github.com/fabian017/Inteligencia-Artifical-2) [(video)](https://youtu.be/zYAhMFVHFlk) [(+info)](https://colab.research.google.com/drive/1CKr5eA08Nc4GssaKXSCj1ni7Ykv3Ty24?usp=sharing)
---

## Ajuste de la calibración durante la recuperación en el sistema óptico CASSI <a name="proy3"></a>

**Autores: Yesid Romario Gualdrón Hurtado (2190052), Ruben Dario Rodríguez Moreno (2181968), Julian Garcia (2180025)**

<img src="https://raw.githubusercontent.com/yromariogh/IA2-Recovery/main/banner.png" style="width:700px;">

**Objetivo:Reconstrucción de imágenes espectrales capturadas con el sistema óptico CASSI considerando el desajuste propio del hardware, mediante redes neuronales profundas.**

- Dataset: NTIRE ARAD 2020
- Modelo: UNet, Convolutional Autoencoders, RNNs y CNNs.

[(code)](https://github.com/yromariogh/IA2-Recovery) [(video)](https://youtu.be/_18skgkLT_I) [(+info)](https://correouisedu-my.sharepoint.com/:p:/g/personal/yesid2190052_correo_uis_edu_co/ERMF25HOtnBDrgFr06yiJzUB90apcaVJQVoSRjruaWVBIg?e=AilP59)
---



## Reconocimiento de las causas de los accidentes cerebrovasculares usando imágenes de coágulos <a name="proy4"></a>

**Autores: Jorge Saul Castillo Jaimes**

<img src="https://gitlab.com/hopkeinst07/proyecto-ai2/-/raw/main/banner.png" style="width:700px;">

**Objetivo: identificar las causas de los accidentes cerebrovasculares, en al menos 3 categorías: CE, LAA y otras; todo esto con la finalidad de que frente a un primer accidente que presente una persona poder brindar un tratamiento terapéutico adecuado y evitar un segundo accidente.**

- Dataset:https://gitlab.com/hopkeinst07/proyecto-ai2/-/tree/main/Dataset
- Modelo:

[(code)](https://gitlab.com/hopkeinst07/proyecto-ai2) [(video)](https://www.youtube.com/watch?v=CG-QkKukAb0) [(+info)](https://gitlab.com/hopkeinst07/proyecto-ai2)
---


