---
# Proyectos 2021-1. Inteligencia Artificial II. 

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [DETECCIÓN DE HELADAS NEGRAS EN EL PARAMO DE BERLIN](#proy1)
2. [Detección de neumonía en imágenes de radiografía de tórax.](#proy2)
3. [Autoencoders para colorear imágenes](#proy3)
4. [Waste Classifier using YOLOv3](#proy4)
5. [Clasificación del comportamiento de conductores](#proy5)
6. [GANs con Imágenes Espectrales](#proy6)
7. [CLASIFICACIÓN DE GÉNEROS MUSICALES](#proy7)



## DETECCIÓN DE HELADAS NEGRAS EN EL PARAMO DE BERLIN<a name="proy1"></a>

**Autores: Angie García Delgado -2172970, Pablo Serrano Rincón - 2172720, Camilo Marín García - 2172969**

<img src="https://github.com/DerekMazino/Deteccion_Heladas_Negras/raw/master/banner.jpg" style="width:700px;">

**Objetivo:Detección de Heladas Negras en el Páramo de Berlín con el fin de minimizar el impacto negativo en los cultivos.**

- Dataset: Variables metereologicas del IDEAM
- Modelo: Red LSTM Multivariable y Autoencoder para detección de anomlias.

[(code)](https://github.com/DerekMazino/Deteccion_Heladas_Negras/blob/master/ProyectoIAa.ipynb) [(video)](https://www.youtube.com/watch?v=Ep_fZO-4v3s) [(+info)](https://github.com/DerekMazino/Deteccion_Heladas_Negras)

---

## Detección de neumonía en imágenes de radiografía de tórax. <a name="proy2"></a>

**Autores: Juan Camilo Londoño Jaimes, Kevin Alonso Luna Bustos**

<img src="https://raw.githubusercontent.com/JuanLondono32/Proyecto-IA2/main/Banner%20Proyecto%20IA2.png" style="width:700px;">

**Objetivo: Aplicar los métodos de Deep Learning para desarrollar un algoritmo de clasificación para determinar la presencia de la neumonía en imágenes de radiografía de tórax.**

- Dataset: CoronaHack -Chest X-Ray-Dataset
- Modelo: CNN, EfficientNetB1, VGG16, ResNet50


[(code)](https://github.com/JuanLondono32/Proyecto-IA2/blob/main/Proyecto_IA2.ipynb) [(video)](https://youtu.be/ondL_fNPtLo) [(+info)](https://github.com/JuanLondono32/Proyecto-IA2)

---
## Autoencoders para colorear imágenes <a name="proy3"></a>

**Autores:  David Rojas, Nicolás Galván, Hazel Pinzón.**

<img src="https://user-images.githubusercontent.com/71358878/137845424-6f4cde35-7a1c-4fe2-a9c6-24bb1c690ca1.png" style="width:700px;">

**Objetivo: Implementar diversos modelos de autoencoders para la coloración de imágenes, de tal manera que podamos traer al futuro las imágenes de nuestro pasado.**

- Dataset: https://www.kaggle.com/theblackmamba31/landscape-image-colorization
- Modelo: modelo-AE-concatL, modelo-AE-noconcatM, ckpt-10

[(code)](https://github.com/hazel1399/ProyectoIA2/blob/main/Proyecto_Final_IA2_resultados_.ipynb) [(video)](https://www.youtube.com/watch?v=Tzk7aIQNArs) [(+info)](https://github.com/hazel1399/ProyectoIA2)
---
## Waste Classifier using YOLOv3 <a name="proy4"></a>

**Autores:Hollman Esteban González Suárez, Jorge Andrés Burgos Fuentes, Sebastian Contreras Ceballos**

<img src="https://github.com/HollmanG/wasteclassifierIA2/raw/main/Banner/Banner.png" style="width:700px;">

**Objetivo:Clasificar residuos mediante Deep Learning utilizando YOLO.**

- Dataset: https://drive.google.com/file/d/1c32dl3U0AstUNxM1RRkatOpREu-RhLp8/view?usp=sharing
- Modelo: YOLOv3, Darknet.



[(code)](https://github.com/HollmanG/wasteclassifierIA2) [(video)](https://youtu.be/ACVzPtJNpVw) [(+info)](https://github.com/HollmanG/wasteclassifierIA2/blob/main/Slides/Waste%20Classifier.pdf)
---

## Clasificación del comportamiento de conductores <a name="proy5"></a>

**Autores: Juan Sebastian Estupiñan Cobos 2180056, Sebastian Rivera León 2180033, Juan Sebastian Trujillo - 2160602**

<img src="https://github.com/Etherion99/IA2_comportamiento_conductores/raw/main/banner.jpg" style="width:700px;">

**Objetivo:brindar una base que permita monitorear el comportamiento de los conductores, esta motivación surge al observar la vida cotidiana, en donde gran variedad de conductores están expuestos a muchas distracciones que desafortunadamente pueden terminar en accidentes**

- Dataset: https://www.kaggle.com/mikoaro/distracteddriver
- Modelo: Modelo VGG16 pre-entrenado con las últimas 2 capas descongeladas y conectado a 2 capas densas de 256 y 128 ambas con activación relu, optimizador Adam.

[(code)](https://github.com/Etherion99/IA2_comportamiento_conductores) [(video)](https://www.youtube.com/watch?v=hc4-Ou34ukk) [(+info)](https://github.com/Etherion99/IA2_comportamiento_conductores/blob/main/presentaci%C3%B3n.pptx)
---

## GANs con Imágenes Espectrales <a name="proy6"></a>

**Autores: Brayan Esneider Monroy Chaparro - 2180034, Geison Alfredo Blanco Rodriguez - 2180045, Ivan David Ortiz Pineda - 2180018**

<img src="https://github.com/bemc22/spectral-gans/raw/main/imgs/banner.png" style="width:700px;">

**Objetivo: Resolver el problema inverso, es decir, a partir de una imagen RGB obtener la imagen espectral asociada, permitiendo generar y construir nuevos conjuntos de datos de imágenes espectrales a partir de imágenes RGB. .**

- Dataset: http://vclab.kaist.ac.kr/siggraphasia2017p1/kaistdataset.html
- Modelo: GAns

[(code)](https://github.com/bemc22/spectral-gans) [(video)](https://drive.google.com/file/d/1n8R0v5nHzd50XxuVdNP4iFXYuNrLz5oO/view) [(+info)]0(https://github.com/bemc22/spectral-gans)
---

## CLASIFICACIÓN DE GÉNEROS MUSICALES <a name="proy7"></a>

**Autores:Andrés Felipe Uribe García, Juan Felipe Ortiz Trillos, Orlando Alberto Moncada Rodríguez**

<img src="" style="width:700px;">

**Objetivo: .**

- Dataset: 
- Modelo: 

[(code)]() [(video)]() [(+info)](https://github.com/LotusZaheer/Proyecto)
---
**Autores: XXXX**

<img src="" style="width:700px;">

**Objetivo: .**

- Dataset: 
- Modelo: 

[(code)]() [(video)]() [(+info)]()
---